﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace NhibernateRepo
{
    public class PostsMap : ClassMapping<Posts>
    {
        public PostsMap()
        {
            Id(x => x.Id, m => m.Generator(Generators.Increment));
            
            Property(x=>x.userId);
            Property(x => x.title);
            Property(x => x.body);
        }
    }
}
