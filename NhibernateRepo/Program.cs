﻿using System;
using System.Linq;
using Newtonsoft.Json;

namespace NhibernateRepo
{
  class Program
  {
    static void Main(string[] args)
    {
      var _personRepo = new NHibernatePostsRepository();
      
      _personRepo.Save(new Posts
      {
        body = "nhib body",
        title = "nhib tile",
        userId = 123
      });
      
      _personRepo.Save(new Posts
      {
        body = "nhib body2",
        title = "nhib tile2",
        userId = 123
      });
      
      
      var posts = _personRepo.Get();
      foreach (var post in posts)
           Console.WriteLine(JsonConvert.SerializeObject(post));

    }
  }
}
