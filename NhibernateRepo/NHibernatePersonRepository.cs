﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;

namespace NhibernateRepo
{
    public class NHibernatePostsRepository
    {
        public Posts Get(long id)
        {
            throw new NotImplementedException();
        }

        public void Save(Posts person)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                session.Save(person);
                transaction.Commit();
            }
        }

        public List<Posts> Get()
        {
            using (ISession session = NHibernateHelper.OpenSession())
                return session.Query<Posts>().ToList();
        }

        public void Update(Posts person)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                session.Update(person);
                transaction.Commit();
            }
        }

        public void Delete(Posts person)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                session.Delete(person);
                transaction.Commit();
            }
        }
        
    }
}