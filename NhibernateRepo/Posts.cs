﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NhibernateRepo
{
  public class Posts
  {
    [Key]
    public virtual long Id { get; set; }
    public virtual long userId { get; set; }
    public virtual string title { get; set; }
    public virtual string body { get; set; }
  }
}
