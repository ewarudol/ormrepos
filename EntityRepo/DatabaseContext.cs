﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace EntityRepo
{
  class PostsContext : DbContext
  {
    public PostsContext()
    {
      Database.EnsureCreated();
    }
    public DbSet<Posts> Posts { get; set; }


    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {

    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
      optionsBuilder.UseSqlite(@"Data Source=testBase.sqlite");
      base.OnConfiguring(optionsBuilder);
    }
    
  }
}
