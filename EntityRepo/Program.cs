﻿using System;
using System.Linq;

namespace EntityRepo
{
  class Program
  {
    static void Main(string[] args)
    {
      using (var context = new PostsContext())
      {
        var posts = context.Posts.ToList();

        context.Posts.Add(new Posts
        {
          title = "entity title",
          body = "entity body",
          userId = 123
        });
        context.SaveChanges();
        
        foreach (var post in posts)
          Console.WriteLine(post.body);
      }
    }
  }
}
