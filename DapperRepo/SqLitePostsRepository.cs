﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using Dapper;
using Dapper.Contrib.Extensions;

namespace DapperRepo
{
    public class SqLitePostsRepository
    {
        public static string DbFile
        {
            get { return "testBase.sqlite"; }
        }

        public static SQLiteConnection SimpleDbConnection()
        {
            return new SQLiteConnection("Data Source=" + DbFile);
        }
        
        public List<Posts> GetPosts()
        {
            if (!File.Exists(DbFile)) return null;

            using (var cnn = SimpleDbConnection())
            {
                cnn.Open();
                List<Posts> result = cnn.GetAll<Posts>().ToList();
                return result;
            }
        }

        public void SavePost(Posts post)
        {
            if (!File.Exists(DbFile))
            {
                Console.WriteLine("nie ma bazy");
            }

            using (var cnn = SimpleDbConnection())
            {
                cnn.Open();
                post.Id = cnn.Query<long>(
                    @"INSERT INTO Posts 
                    ( title, body, userID ) VALUES 
                    ( @title, @body, @userId );
                    select last_insert_rowid()", post).First();
            }
        }
        
    }
}
