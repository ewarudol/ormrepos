﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DapperRepo
{
    class Program
    {
        static void Main(string[] args)
        {
            var rep = new SqLitePostsRepository();
            var newpost = new Posts
                {
                    title = "dapper title",
                    body = "dapper body",
                    userId = 123
                };
            rep.SavePost(newpost);

            var posts = rep.GetPosts();
            foreach (var post in posts)
                Console.WriteLine(post.body);
        }
    }
}
