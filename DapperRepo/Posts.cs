﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DapperRepo
{
  [Table("Posts")]
  public sealed class Posts
  {
    [Key]
    public long Id { get; set; }
    public long userId { get; set; }
    public string title { get; set; }
    public string body { get; set; }
  }
}
